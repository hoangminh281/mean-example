"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
exports.CampaignSchema = new Schema({
    account: {
        type: mongoose.Schema.Types.ObjectId,
        required: 'Enter a account'
    },
    name: {
        type: String,
        required: 'Enter a name'
    },
    created: {
        type: Date,
        required: 'Enter a created'
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: 'Enter a created by'
    },
    modified: {
        type: Date,
        required: 'Enter a modified'
    },
    modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: 'Enter a modified by'
    },
    statistics: {
        channels: {
            type: Number,
            default: 0
        },
        emails_recipients: {
            type: Number,
            default: 0
        },
        emails_sent: {
            type: Number,
            default: 0
        },
        sms_recipients: {
            type: Number,
            default: 0
        },
        sms_sent: {
            type: Number,
            default: 0
        },
        sms: Number
    }
});
//# sourceMappingURL=campaignModel.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    account: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Account',
            unique: true,
            required: true
        }]
});
//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this;
    if (!this.isModified('password')) {
        return next();
    }
    next();
    // bcrypt.genSalt(10, (err, salt) => {
    //     if (err) {
    //         return next(err);
    //     }
    //     console.log(salt)
    //     bcrypt.hash(user.password, salt, null, function (err, hash) {
    //         if (err) {
    //             return next(err);
    //         }
    //         console.log(hash)
    //         user.password = hash;
    //         next();
    //     });
    // });
});
//authenticate input against database
UserSchema.statics.authenticate = function (email, password, callback) {
    User.findOne({ email })
        .exec(function (error, user) {
        if (error) {
            return callback(error);
        }
        else if (!user) {
            var err = new Error('User not found.');
            err.status = 401;
            return callback(err);
        }
        // bcrypt.compare(password, user.password, function (err, result) {
        //     console.log(password, user.password)
        //     if (result === true) {
        //         return callback(null, user);
        //     } else {
        //         return callback();
        //     }
        // });
        if (password === user.password) {
            return callback(null, user);
        }
        else {
            return callback();
        }
    });
};
var User = mongoose.model('User', UserSchema);
exports.default = User;
//# sourceMappingURL=userModel.js.map
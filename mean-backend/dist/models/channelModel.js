"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
exports.ChannelSchema = new Schema({
    campaign: {
        type: mongoose.Schema.Types.ObjectId,
        required: 'Enter a account'
    },
    type: {
        type: String,
        required: 'Enter a type',
        enum: ['email', 'sms']
    },
    from: {
        type: String,
        required: 'Enter a from'
    },
    from_name: {
        type: String,
        required: 'Enter a from name'
    },
    subject: {
        type: String,
        required: 'Enter a subject'
    },
    content: {
        type: String,
        required: 'Enter a content'
    },
    created: {
        type: Date,
        required: 'Enter a created'
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: 'Enter a created by'
    },
    modified: {
        type: Date,
        required: 'Enter a modified'
    },
    modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: 'Enter a modified by'
    },
    sent: {
        type: Date,
        required: 'Enter a sent'
    },
    // search: {
    //     join: "AND",
    //         fields : [
    //             {
    //                 field: "email", String  Reference to field being searched
    //             fieldType: "string", String  Type of search field
    //             comparison : "equals", String
    //             value : "datrong@tma.com.vn"  String  Search value
    //             }
    //         ]
    // },
    statistics: {
        emails_sent: {
            type: Number,
            default: 0
        },
        sms_sent: {
            type: Number,
            default: 0
        },
    }
});
//# sourceMappingURL=channelModel.js.map
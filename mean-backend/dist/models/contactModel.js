"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
exports.ContactSchema = new Schema({
    email: {
        type: String,
        required: this.mobile ? false : true
    },
    mobile: {
        type: String,
        required: this.email ? false : true
    },
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    birthday: {
        type: Date,
        required: true
    },
    gender: {
        type: String,
        required: true,
        enum: ['male', 'female', 'other']
    },
    hobbies: {
        type: Array,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    modified: {
        type: Date,
        required: true
    },
    modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});
//# sourceMappingURL=contactModel.js.map
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
var AccountSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    country: {
        type: String,
        required: true
    },
    timezone: {
        type: String,
        required: true
    },
    currency: {
        type: String,
        required: true
    },
    monthly_subscription_fee: {
        type: String,
        required: true
    },
    email_inclusion: {
        type: Number,
        default: 0
    },
    sms_inclusion: {
        type: Number,
        default: 0
    },
    contact_inclusion: {
        type: Number,
        default: 0
    },
    email_fee: {
        type: Number,
        required: true
    },
    sms_fee: {
        type: Number,
        required: true
    },
    contact_fee: {
        type: Number,
        required: true
    },
    startup_fee: {
        type: Number,
        default: 0
    },
    status: {
        type: Number,
        default: 1,
    },
    parent: mongoose.Schema.Types.ObjectId
});
var Account = mongoose.model('Account', AccountSchema);
exports.default = Account;
//# sourceMappingURL=accountModel.js.map
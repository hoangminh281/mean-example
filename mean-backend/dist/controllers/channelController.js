"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const channelModel_1 = require("../models/channelModel");
const Channel = mongoose.model('Channel', channelModel_1.ChannelSchema);
class ChannelController {
    getChannels(req, res) {
        Channel.find({}, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }
    addNewChannel(req, res) {
        let newChannel = new Channel(req.body);
        newChannel.save((err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }
    getChannelWithID(req, res) {
        Channel.findById(req.params.channelId, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }
    updateChannel(req, res) {
        Channel.findOneAndUpdate({ _id: req.params.channelId }, req.body, { new: true }, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }
    deleteChannel(req, res) {
        Channel.deleteOne({ _id: req.params.channelId }, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted channel!' });
        });
    }
}
exports.ChannelController = ChannelController;
//# sourceMappingURL=channelController.js.map
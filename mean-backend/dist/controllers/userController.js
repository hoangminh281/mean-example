"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const userModel_1 = require("../models/userModel");
class UserController {
    authenticate(req, res) {
        let email = req.body.email, password = req.body.password;
        userModel_1.default.authenticate(email, password, function (error, user) {
            if (error || !user) {
                var err = new Error('Wrong email or password');
                err.status = 401;
                res.send(err);
            }
            res.json(user);
        });
    }
    getUsers(req, res) {
        userModel_1.default.find({}, (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
    addNewUser(req, res) {
        let newUser = new userModel_1.default(req.body);
        newUser.save((err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
    getUserWithID(req, res) {
        userModel_1.default.findById(req.params.userId, (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
    updateUser(req, res) {
        userModel_1.default.findOneAndUpdate({ _id: req.params.userId }, req.body, { new: true }, (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }
    deleteUser(req, res) {
        userModel_1.default.deleteOne({ _id: req.params.userId }, (err, user) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted user!' });
        });
    }
    findByOption(req, res) {
        const filledOption = {
            _id: req.body.option.id
        };
        if (!filledOption) {
            var err = new Error('Request empty or not validated');
            err.status = 401;
            res.send(err);
        }
        userModel_1.default.find(filledOption)
            .select({ _id: 0, account: 1 })
            .populate('account')
            .exec((err, result) => {
            if (err) {
                res.send(err);
            }
            res.json(result[0] ? result[0].account : '');
        });
    }
}
exports.UserController = UserController;
//# sourceMappingURL=userController.js.map
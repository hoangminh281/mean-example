"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const accountModel_1 = require("../models/accountModel");
const userModel_1 = require("../models/userModel");
class AccountController {
    getAccounts(req, res) {
        accountModel_1.default.find({}, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json(account);
        });
    }
    addNewAccount(req, res) {
        let newAccount = new accountModel_1.default(req.body.account), account;
        return newAccount.save()
            .then((err, _account) => {
            if (err) {
                res.send(err);
            }
            account = _account;
            let newUser = new userModel_1.default(Object.assign({}, req.body.user, { "firstname": req.body.account.accountName, "lastname": '123', "password": '123456', "account": account._id }));

            return newUser.save();
        })
            .then((err, user) => {
            if (err) {
                res.send(err);
            }

            return res.json({
                account,
                user
            });
        });
    }
    getAccountWithID(req, res) {
        accountModel_1.default.findById(req.params.accountId, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json(account);
        });
    }
    updateAccount(req, res) {
        accountModel_1.default.findOneAndUpdate({ _id: req.params.accountId }, req.body, { new: true }, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json(account);
        });
    }
    deleteAccount(req, res) {
        accountModel_1.default.deleteOne({ _id: req.params.accountId }, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted account!' });
        });
    }
}
exports.AccountController = AccountController;
//# sourceMappingURL=accountController.js.map
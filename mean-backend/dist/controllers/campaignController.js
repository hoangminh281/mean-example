"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const campaignModel_1 = require("../models/campaignModel");
const Campaign = mongoose.model('Campaign', campaignModel_1.CampaignSchema);
class CampaignController {
    getCampaigns(req, res) {
        Campaign.find({}, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }
    addNewCampaign(req, res) {
        let newCampaign = new Campaign(req.body);
        newCampaign.save((err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }
    getCampaignWithID(req, res) {
        Campaign.findById(req.params.campaignId, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }
    updateCampaign(req, res) {
        Campaign.findOneAndUpdate({ _id: req.params.campaignId }, req.body, { new: true }, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }
    deleteCampaign(req, res) {
        Campaign.deleteOne({ _id: req.params.campaignId }, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted campaign!' });
        });
    }
}
exports.CampaignController = CampaignController;
//# sourceMappingURL=campaignController.js.map
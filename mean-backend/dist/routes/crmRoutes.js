"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const userController_1 = require("../controllers/userController");
const accountController_1 = require("../controllers/accountController");
const contactController_1 = require("../controllers/contactController");
const channelController_1 = require("../controllers/channelController");
const campaignController_1 = require("../controllers/campaignController");
class Routes {
    constructor() {
        this.userController = new userController_1.UserController();
        this.accountController = new accountController_1.AccountController();
        this.contactController = new contactController_1.ContactController();
        this.channelController = new channelController_1.ChannelController();
        this.campaignController = new campaignController_1.CampaignController();
    }
    routes(app) {
        app.route('/')
            .get((req, res) => {
            res.status(200).send({
                message: 'Get request successfull!!!!'
            });
        });
        //authen
        app.route('/signin')
            .post(this.userController.authenticate);
        //account
        app.route('/account')
            .get(this.accountController.getAccounts)
            .post(this.accountController.addNewAccount);
        //get a specific account
        app.route('/account/:accountId')
            .get(this.accountController.getAccountWithID)
            .put(this.accountController.updateAccount)
            .delete(this.accountController.deleteAccount);
        //user
        app.route('/user')
            .get(this.userController.getUsers)
            .post(this.userController.addNewUser);
        //get a specific user
        app.route('/user/:userId')
            .get(this.userController.getUserWithID)
            .put(this.userController.updateUser)
            .delete(this.userController.deleteUser);
        //find by option
        app.route('/user/find')
            .post(this.userController.findByOption);
        //contact
        app.route('/contact')
            .get(this.contactController.getContacts)
            .post(this.contactController.addNewContact);
        //get a specific contact
        app.route('/contact/:contactId')
            .get(this.contactController.getContactWithID)
            .put(this.contactController.updateContact)
            .delete(this.contactController.deleteContact);
        //compaign
        app.route('/campaign')
            .get(this.campaignController.getCampaigns)
            .post(this.campaignController.addNewCampaign);
        //get a specific compaign
        app.route('/campaign/:campaignId')
            .get(this.campaignController.getCampaignWithID)
            .put(this.campaignController.updateCampaign)
            .delete(this.campaignController.deleteCampaign);
        //channel
        app.route('/channel')
            .get(this.channelController.getChannels)
            .post(this.channelController.addNewChannel);
        //get a specific channel
        app.route('/channel/:channelId')
            .get(this.channelController.getChannelWithID)
            .put(this.channelController.updateChannel)
            .delete(this.channelController.deleteChannel);
    }
}
exports.Routes = Routes;
//# sourceMappingURL=crmRoutes.js.map
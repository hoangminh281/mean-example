import { Request, Response } from "express";

import { UserController } from '../controllers/userController';
import { AccountController } from '../controllers/accountController';
import { ContactController } from '../controllers/contactController';
import { ChannelController } from '../controllers/channelController';
import { CampaignController } from '../controllers/campaignController';
import { AuthenticateController } from "../controllers/authenticateController";

export class Routes {
    public userController: UserController = new UserController();
    public accountController: AccountController = new AccountController();
    public contactController: ContactController = new ContactController();
    public channelController: ChannelController = new ChannelController();
    public campaignController: CampaignController = new CampaignController();
    public authenticateController: AuthenticateController = new AuthenticateController();

    public routes(app): void {
        app.route('/')
            .get((req: Request, res: Response) => {
                res.status(200).send({
                    message: 'Get request successfull!!!!'
                });
            });

        //account
        app.route('/account')
            .get(this.accountController.getAccounts.bind(this.accountController))
            .post(this.accountController.addNewAccount.bind(this.accountController));

        //get a specific account
        app.route('/account/:accountId')
            .get(this.accountController.getAccountById.bind(this.accountController))
            .put(this.accountController.updateAccount.bind(this.accountController))
            .delete(this.accountController.deleteAccount.bind(this.accountController));

        app.route('/account/:accountId/child')
            .get(this.accountController.getChildAccountById.bind(this.accountController));

        //user
        app.route('/user')
            .get(this.userController.getUsers)
            .post(this.userController.addNewUser);

        //get a specific user
        app.route('/user/:userId')
            .get(this.userController.getUserById)
            .put(this.userController.updateUser.bind(this.userController))
            .delete(this.userController.deleteUser);

        //find by option
        app.route('/user/find')
            .post(this.userController.findByOptions);

        //contact
        app.route('/contact')
            .get(this.contactController.getContacts)
            .post(this.contactController.addNewContact);

        app.route('/contact/find')
            .post(this.contactController.findByOptions);

        //get a specific contact
        app.route('/contact/:contactId')
            .get(this.contactController.getContactById)
            .put(this.contactController.updateContact)
            .delete(this.contactController.deleteContact);

        //compaign
        app.route('/campaign')
            .get(this.campaignController.getCampaigns)
            .post(this.campaignController.addNewCampaign);

        //get a specific compaign
        app.route('/campaign/:campaignId')
            .get(this.campaignController.getCampaignById)
            .put(this.campaignController.updateCampaign)
            .delete(this.campaignController.deleteCampaign);

        //channel
        app.route('/channel')
            .get(this.channelController.getChannels)
            .post(this.channelController.addNewChannel);

        //get a specific channel
        app.route('/channel/:channelId')
            .get(this.channelController.getChannelById)
            .put(this.channelController.updateChannel)
            .delete(this.channelController.deleteChannel);

        //authentication
        app.route('/auth/signin')
            .post(this.authenticateController.authenticate.bind(this.authenticateController));
        app.route('/auth/authenticate2fa')
            .post(this.authenticateController.authenticate2FA.bind(this.authenticateController));
    }
}
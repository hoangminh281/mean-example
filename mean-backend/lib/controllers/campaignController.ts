import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import Campaign from '../models/campaignModel';

export class CampaignController {
    public getCampaigns(req: Request, res: Response) {
        Campaign.find({}, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }

    public addNewCampaign(req: Request, res: Response) {
        let newCampaign = new Campaign(req.body);

        newCampaign.save((err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }

    public getCampaignById(req: Request, res: Response) {
        Campaign.findById(req.params.campaignId, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }

    public updateCampaign(req: Request, res: Response) {
        Campaign.findOneAndUpdate({ _id: req.params.campaignId }, req.body, { new: true }, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json(campaign);
        });
    }

    public deleteCampaign(req: Request, res: Response) {
        Campaign.deleteOne({ _id: req.params.campaignId }, (err, campaign) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted campaign!' });
        });
    }
}
import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import Account from '../models/accountModel';
import User from '../models/userModel';
import * as _ from 'lodash';
import * as speakeasy from 'speakeasy';

import { UserRepository } from '../repositories/userRepository'

export class AuthenticateController {
    private userRepository: UserRepository;

    constructor() {
        this.userRepository = new UserRepository();
    }

    public authenticate(req: Request, res: Response) {
        let email = req.body.email,
            password = req.body.password;

        this.userRepository.findByEmail(email)
            .then((user, err) => {
                if (err || _.isEmpty(user)) {
                    var err: any = new Error('Wrong email or password');
                    err.status = 401;

                    res.send(err);
                }

                if (password === user.password) {
                    user.is2FA = _.isEmpty(user.secret) ? false : true;
                    _.unset(user, "secret");

                    res.json(user);
                }

                res.json();
            });
    }

    public authenticate2FA(req: Request, res: Response) {
        let userId = req.body.userId,
            code2FA = req.body.code2FA;
        this.userRepository.findById(userId)
            .then((user, err) => {
                if (err || _.isEmpty(user)) {
                    var err: any = new Error('User not found');
                    err.status = 401;

                    res.send(err);
                }

                let verified = speakeasy.totp.verify({
                    secret: user.secret,
                    encoding: 'base32',
                    token: code2FA
                });

                res.json(verified);
            });

    }

}
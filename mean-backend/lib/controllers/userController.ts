import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { Request, Response } from 'express';
import User from '../models/userModel';
import { UserRepository } from '../repositories/userRepository'

export class UserController {
    private userRepository: UserRepository;

    constructor() {
        this.userRepository = new UserRepository();
    }

    public getUsers(req: Request, res: Response) {
        User.find({}, (err, user) => {
            if (err) {
                res.send(err);
            }

            res.json(user);
        });
    }

    public addNewUser(req: Request, res: Response) {
        let newUser = new User({
            "firstname": req.body.firstname || '',
            "lastname": req.body.lastname || '',
            "email": req.body.email,
            "password": req.body.password || '123456',
            "account": req.body.account
        });

        newUser.save((err, user) => {
            if (err) {
                res.send(err);
            }

            res.json(user);
        });
    }

    public getUserById(req: Request, res: Response) {
        User.findById(req.params.userId, (err, user) => {
            if (err) {
                res.send(err);
            }

            res.json(user);
        });
    }

    public updateUser(req: Request, res: Response) {
        let userId = req.params.userId,
            updateObject = {
                $set: req.body
            };

        return this.userRepository.update(userId, updateObject)
            .then((user, err) => {
                if (err) {
                    res.send(err);
                }

                res.json(user);
            });
    }

    public deleteUser(req: Request, res: Response) {
        User.deleteOne({ _id: req.params.userId }, (err, user) => {
            if (err) {
                res.send(err);
            }

            res.json({ messase: 'Successfully deleted user!' });
        });
    }

    public findByOptions(req: Request, res: Response) {
        const options = req.body;

        if (_.isEmpty(options)) {
            var err: any = new Error('Request empty or not validated');
            err.status = 401;

            res.send(err);
        }

        User.find(options)
            .select({ secret: 0 })
            .exec((err, result) => {
                if (err) {
                    res.send(err);
                }

                res.json(result);
            });
    }
}
import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import Account from '../models/accountModel';
import User from '../models/userModel';
import { AccountRepository } from '../repositories/accountRepository';

export class AccountController {
    private accountRepository: AccountRepository;

    constructor() {
        this.accountRepository = new AccountRepository();
    }

    public getAccounts(req: Request, res: Response) {
        Account.find({}, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json(account);
        });
    }

    public addNewAccount(req: Request, res: Response) {
        let newAccount = new Account(req.body.account),
            account;

        return newAccount.save()
            .then((_account) => {
                account = _account;

                let newUser = new User({
                    "email": req.body.user.email,
                    "firstname": account.name,
                    "lastname": '123',
                    "password": '123456',
                    "account": account._id
                });

                return newUser.save();
            })
            .then((user) => {
                return res.json({
                    account,
                    user
                });
            })
            .catch((err) => {
                return res.send(err);
            });
    }

    public getAccountById(req: Request, res: Response) {
        Account.findById(req.params.accountId, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json(account);
        });
    }

    public updateAccount(req: Request, res: Response) {
        let accountId = req.params.accountId,
            updateObject = {
                $set: req.body
            };

        return this.accountRepository.update(accountId, updateObject)
            .then((account, err) => {
                if (err) {
                    res.send(err);
                }

                res.json(account);
            });
    }

    public deleteAccount(req: Request, res: Response) {
        Account.deleteOne({ _id: req.params.accountId }, (err, account) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted account!' });
        });
    }

    public getChildAccountById(req: Request, res: Response) {
        let accountId = req.params.accountId;

        this.accountRepository.findChildByAccountId(accountId)
            .then((accounts: any) => {
                const promises = accounts.map(account => {
                    return this.accountRepository.countChildByAccountId(account._id)
                        .then((res) => {
                            return {
                                ...account,
                                child_count: res
                            };
                        })
                        .catch((err) => {
                            return {
                                ...account,
                                child_count: 0
                            };
                        });
                });

                return Promise.all(promises)
                    .then((countedAccounts) => {
                        res.json(countedAccounts);
                    });
            })
            .catch((err) => {
                res.send(err);
            });
    }
}
import * as mongoose from 'mongoose';
import { Request, Response } from 'express';
import Channel from '../models/channelModel';

export class ChannelController {
    public getChannels(req: Request, res: Response) {
        Channel.find({}, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }

    public addNewChannel(req: Request, res: Response) {
        let newChannel = new Channel(req.body);

        newChannel.save((err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }

    public getChannelById(req: Request, res: Response) {
        Channel.findById(req.params.channelId, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }

    public updateChannel(req: Request, res: Response) {
        Channel.findOneAndUpdate({ _id: req.params.channelId }, req.body, { new: true }, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json(channel);
        });
    }

    public deleteChannel(req: Request, res: Response) {
        Channel.deleteOne({ _id: req.params.channelId }, (err, channel) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted channel!' });
        });
    }
}
import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { Request, Response } from 'express';
import Contact from '../models/contactModel';

export class ContactController {
    public getContacts(req: Request, res: Response) {
        Contact.find({}, (err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public addNewContact(req: Request, res: Response) {
        let newContact = new Contact(req.body);

        newContact.save((err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public getContactById(req: Request, res: Response) {
        Contact.findById(req.params.contactId, (err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public updateContact(req: Request, res: Response) {
        Contact.findOneAndUpdate({ _id: req.params.contactId }, req.body, { new: true }, (err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json(contact);
        });
    }

    public deleteContact(req: Request, res: Response) {
        Contact.deleteOne({ _id: req.params.contactId }, (err, contact) => {
            if (err) {
                res.send(err);
            }
            res.json({ messase: 'Successfully deleted contact!' });
        });
    }

    public findByOptions(req: Request, res: Response) {
        const options = req.body;

        if (_.isEmpty(options)) {
            var err: any = new Error('Request empty or not validated');
            err.status = 401;

            res.send(err);
        }

        Contact.find(options)
            .exec((err, result) => {
                if (err) {
                    res.send(err);
                }

                res.json(result);
            });
    }
}
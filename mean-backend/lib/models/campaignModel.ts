import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

var CampaignSchema = new Schema({
    account: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
        required: true
    },
    name: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    modified: {
        type: Date,
        required: true
    },
    modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    statistics: {
        channels: {
            type: Number,
            default: 0
        },
        emails_recipients: {
            type: Number,
            default: 0
        },
        emails_sent: {
            type: Number,
            default: 0
        },
        sms_recipients: {
            type: Number,
            default: 0
        },
        sms_sent: {
            type: Number,
            default: 0
        },
        sms: Number
    }
});

var Campaign = mongoose.model('Campaign', CampaignSchema);

export default Campaign;
import * as mongoose from 'mongoose';
import * as _ from 'lodash';

const Schema = mongoose.Schema;

var UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        trim: true,
        required: true
    },
    firstname: {
        type: String
    },
    lastname: {
        type: String
    },
    password: {
        type: String,
        required: true
    },
    secret: {
        type: String
    },
    account: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
        required: true
    }
});

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this;

    if (!this.isModified('password')) {
        return next();
    }
    next();
});

var User = mongoose.model('User', UserSchema);

export default User;

import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

var ChannelSchema = new Schema({
    campaign: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Campaign',
        required: true
    },
    type: {
        type: String,
        required: true,
        enum: ['email', 'sms']
    },
    from: {
        type: String,
        required: true
    },
    from_name: {
        type: String,
        required: true
    },
    subject: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        required: true
    },
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    modified: {
        type: Date,
        required: true
    },
    modified_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    sent: {
        type: Date,
        required: true
    },
    // search: {
    //     join: "AND",
    //         fields : [
    //             {
    //                 field: "email", String  Reference to field being searched
    //             fieldType: "string", String  Type of search field
    //             comparison : "equals", String
    //             value : "datrong@tma.com.vn"  String  Search value
    //             }
    //         ]
    // },
    statistics: {
        emails_sent: {
            type: Number,
            default: 0
        },
        sms_sent: {
            type: Number,
            default: 0
        },
    }
});

var Channel = mongoose.model('Channel', ChannelSchema);

export default Channel;
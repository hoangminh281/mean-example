import Account from '../models/accountModel';

export class AccountRepository {
    public findById(id) {
        return Account.findOne({ _id: id })
            .lean(true)
            .then((account) => {
                return account;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }

    public update(accountId, updateObject) {
        return Account.findByIdAndUpdate(accountId, updateObject, {
            new: true
        })
            .then((account) => {
                return account;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }

    public findChildByAccountId(accountId) {
        return Account.find({ parent: accountId })
            .lean(true)
            .then((account) => {
                return account;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }

    public countChildByAccountId(accountId) {
        return Account.count({ parent: accountId })
            .lean(true)
            .then((count) => {
                return count;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }
}
import User from '../models/userModel';

export class UserRepository {
    public findById(id) {
        return User.findOne({ _id: id })
            .populate('account')
            .lean(true)
            .then((user) => {
                return user;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }

    public findByEmail(email) {
        return User.findOne({ email })
            .populate('account')
            .lean(true)
            .then((user) => {
                return user;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }

    public update(userId, updateObject) {
        return User.findByIdAndUpdate(userId, updateObject, {
            new: true
        })
            .then((user) => {
                return user;
            })
            .catch((err) => {
                if (err) {
                    throw err;
                }
            });
    }
}
import _ from 'lodash';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material";
import { UserService } from '../../../services/user.service';
import { AddUserDialogComponent } from '../../dialogs/add-user-dialog/add-user-dialog.component';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {
  private userDataTable: any = {
    headers: ['name', 'last login date', 'email', 'role'],
    values: []
  };
  private searchFields: any = ['email', 'name'];
  private userId: any;
  private accountId: any;

  constructor(private userService: UserService, private accountService: AccountService, private router: Router, private route: ActivatedRoute, private dialog: MatDialog) { }

  ngOnInit() {
    this.userId = window.localStorage.getItem('userId');
    this.accountId = window.localStorage.getItem('accountId');

    this.userService.findByOptions({ account: this.accountId }).subscribe((res: any) => {
      this.prepareUserDataTable(res)
    });
  }

  prepareUserDataTable(users: Array<any>) {
    users.forEach(user => {
      this.userDataTable.values.push({
        id: user._id,
        cells: [user.firstname + " " + user.lastname, 'DD/MM/YYYY hh:mm AM/PM', user.email, 'role'],
        status: user.status
      });
    });
  }

  onSearch(data) {
    const emptyFiltedData = _.pickBy(data, _.identity);

    if (!_.isEmpty(emptyFiltedData)) {
      this.userService.findByOptions(emptyFiltedData).subscribe((res: any) => {
        this.userDataTable.values = [];
        this.prepareUserDataTable(res);
      });
    }
  }

  openAddUserDialog() {
    this.dialog.open(AddUserDialogComponent, {
      autoFocus: false,
      data: {
        email: '',
        role: '',
      }
    }).afterClosed().subscribe(result => {
      const newUser = {
        email: result.email,
        account: this.accountId
      };
      this.userService.create(newUser).subscribe((res: any) => {
      });
    });
  }

}

import _ from 'lodash';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from "@angular/material";
import { AddContactDialogComponent } from '../../dialogs/add-contact-dialog/add-contact-dialog.component';
import { ContactService } from 'src/app/services/contact.service';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.scss']
})
export class ContactPageComponent implements OnInit {
  private contactDataTable: any = {
    headers: ['email', 'mobile', 'first name', 'surname', 'more'],
    values: []
  };
  private userId: any;

  constructor(private contactService: ContactService, private router: Router, private route: ActivatedRoute, private dialog: MatDialog) { }

  ngOnInit() {
    this.userId = window.localStorage.getItem('userId');
    this.contactService.findByOptions({
      created_by: this.userId
    }).subscribe((res: any) => {
      this.prepareAccountDataTable(res);
    });
  }

  prepareAccountDataTable(contacts: Array<any>) {
    contacts.forEach(contact => {
      this.contactDataTable.values.push({
        id: contact._id,
        cells: [contact.email, contact.mobile, contact.firstname, contact.lastname],
        // status: account.status
      });
    });
  }

  openAddContactDialog() {
    this.dialog.open(AddContactDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: 'max-width: none !important',
      disableClose: true,
      autoFocus: false,
      hasBackdrop: false,
      data: {
        firstName: '',
        surname: '',
        email: '',
        mobile: '',
        dateOfBirth: '',
        gender: '',
        hobbies: '',
        date: ''
      }
    }).afterClosed().subscribe(result => {
      const newContact = {
        email: result.email,
        mobile: result.mobile,
        firstname: result.firstName,
        lastname: result.surname,
        birthday: result.dateOfBirth,
        gender: result.gender.toLowerCase(),
        hobbies: result.hobbies,
        created: moment(),
        created_by: this.userId,
        modified: moment(),
        modified_by: this.userId
      }
      this.contactService.create(newContact).subscribe((res) => {

      });
    });
  }

}

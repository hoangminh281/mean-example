import { Component, OnInit } from '@angular/core';
import { MatDialog } from "@angular/material";
import { AddCampaignDialogComponent } from '../../dialogs/add-campaign-dialog/add-campaign-dialog.component';
import { AddCampaignWizardComponent } from '../../forms/add-campaign-wizard/add-campaign-wizard.component';

@Component({
  selector: 'app-campaign-page',
  templateUrl: './campaign-page.component.html',
  styleUrls: ['./campaign-page.component.scss']
})
export class CampaignPageComponent implements OnInit {
  private isView: boolean = false;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openAddCampaignDialog() {
    this.dialog.open(AddCampaignDialogComponent, {
      autoFocus: false,
      data: {
        type: '',
        folder: 'create',
        name: ''
      }
    }).afterClosed().subscribe(result => {
      this.dialog.open(AddCampaignWizardComponent, {
        width: '100%',
        height: '100%',
        maxWidth: 'max-width: none !important',
        disableClose: true,
        autoFocus: false,
        hasBackdrop: false,
        data: {
          nameSubject: '',
          nameFrom: '',
          emailFrom: '',
          emailReplies: '',
          isSend: false
        }
      }).afterClosed().subscribe(result => {
      });
    });
  }

  onViewChanged(isView) {
    this.isView = !this.isView;
  }
}

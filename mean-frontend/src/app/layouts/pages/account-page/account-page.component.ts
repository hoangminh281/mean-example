
import * as _ from 'lodash';
import * as from from 'rxjs';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AddAccountDialogComponent } from '../../dialogs/add-account-dialog/add-account-dialog.component';
import { AccountService } from 'src/app/services/account.service';
import { MatDialog } from "@angular/material";
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-account-page',
  templateUrl: './account-page.component.html',
  styleUrls: ['./account-page.component.scss']
})
export class AccountPageComponent implements OnInit {
  private statistics: any = [];
  private accountDataTable: any = {
    headers: ['account name', 'last accessed', 'last activity'],
    values: []
  };

  private userId: string;
  private accountId: string;
  private accounts: any;
  private is2FA: boolean = false;
  private accountNumber: number;
  private totalAccounts: any = 0;
  private activeAccounts: any = 0;
  private monthlySubscriptions: any = 0;

  private actions = ['ACCESS', 'EDIT'];

  constructor(private userService: UserService, private accountService: AccountService, private router: Router, private route: ActivatedRoute, private dialog: MatDialog) { }

  ngOnInit() {
    this.userId = window.localStorage.getItem('userId');
    this.accountId = window.localStorage.getItem('accountId');

    this.accountService.findById(this.accountId).subscribe((res: any) => {
      this.accounts = [res];
      window.localStorage.setItem('accountNumber', this.accounts.length.toString());
      this.accountDataTable.values = this.prepareAccountDataTable(this.accounts);
      this.prepareStatistics(this.accounts);
    });
  }

  prepareAccountDataTable(accounts: Array<any>) {
    return accounts.map(account => {
      return {
        id: account._id,
        cells: [account.name, 'DD/MM/YYYY hh:mm AM/PM', 'Not Data'],
        childCound: account.child_count,
        status: account.status
      };
    });
  }

  prepareStatistics(accounts: Array<any>) {
    accounts.forEach(account => {
      this.totalAccounts++;
      this.activeAccounts += account.status === 1 ? 1 : 0;
      this.monthlySubscriptions += parseInt(account.monthly_subscription_fee) || 0;
    });

    this.statistics = [
      {
        header: 'total accounts',
        value: this.totalAccounts
      },
      {
        header: 'active accounts',
        value: this.activeAccounts
      },
      {
        header: 'monthly subscriptions',
        value: this.prepareCurrency(this.monthlySubscriptions)
      }
    ];
  }

  prepareCurrency(value) {
    const val = String(value);
    let pointingPartVals = val.split('.');
    let preparedValue = "";
    let decimalLength = pointingPartVals[0].length;

    for (let i = 0; i < decimalLength / 3; i++) {
      if (i === 0) {
        preparedValue = pointingPartVals[0].slice(decimalLength - (i * 3 + 3) < 0 ? 0 : decimalLength - (i * 3 + 3), decimalLength - i * 3) + preparedValue;
      }
      else {
        preparedValue = pointingPartVals[0].slice(decimalLength - (i * 3 + 3) < 0 ? 0 : decimalLength - (i * 3 + 3), decimalLength - i * 3) + ',' + preparedValue;
      }
    }

    return '$' + preparedValue + '.' + (pointingPartVals[1] || '00');
  }

  openAddAccountDialog() {
    this.dialog.open(AddAccountDialogComponent, {
      width: '100%',
      height: '100%',
      maxWidth: 'max-width: none !important',
      disableClose: true,
      autoFocus: false,
      hasBackdrop: false,
      data: {
        accountName: '',
        ownerEmail: '',
        country: '',
        timezone: '',

        currency: '',
        monthlySubscriptionFee: '',
        startupFee: '',
        emailInclusion: '',
        smsInclusion: '',
        contactInclusion: '',
        payBy: '',
        emailBasePrice: '',
        smsBasePrice: '',
        contactBasePrice: '',
        parent: this.accountId,
        options: {
          header: 'Create a new account',
          actionType: 'add'
        }
      }
    }).afterClosed().subscribe(result => {
      if (result && result.options.actionType === 'add') {
        _.unset(result, 'options');

        this.accountService.create(result).subscribe(res => {
        });
      }
    });
  }

  onAction(actionObject) {
    if (this.actions[actionObject.actionIndex] === "EDIT") {
      from.forkJoin(
        this.accountService.findById(actionObject.id),
        this.userService.findByOptions({
          account: actionObject.id
        })
      ).subscribe((res: any) => {
        if (res && res.length === 2) {
          const account = res[0];
          const user = res[1][0];

          console.log(account, user)

          this.dialog.open(AddAccountDialogComponent, {
            width: '100%',
            height: '100%',
            maxWidth: 'max-width: none !important',
            disableClose: true,
            autoFocus: false,
            hasBackdrop: false,
            data: {
              accountName: account.name,
              ownerEmail: user.email,
              country: account.country,
              timezone: account.timezone,

              currency: account.currency,
              monthlySubscriptionFee: account.monthly_subscription_fee,
              startupFee: account.startup_fee,
              emailInclusion: account.email_inclusion,
              smsInclusion: account.sms_inclusion,
              contactInclusion: account.contact_inclusion,
              payBy: 'email/sms',
              emailBasePrice: account.email_fee,
              smsBasePrice: account.sms_fee,
              contactBasePrice: account.contact_fee,
              parent: account.parent,
              options: {
                header: 'Edit an account',
                actionType: 'edit'
              }
            }
          }).afterClosed().subscribe(result => {
            if (result && result.options.actionType === 'edit') {
              _.unset(result, 'options');

              this.accountService.update(account._id, result).subscribe(res => {
              });
              this.userService.update(user._id, { email: result.ownerEmail }).subscribe((res) => {
              });
            }
          });
        }
      })
    }
  }

  onChild(accountId) {
    this.accountService.getChildAccount(accountId).subscribe((res: any) => {
      const childAccounts = this.prepareAccountDataTable(res);
      this.accountDataTable.values.forEach((account) => {
        if (account.id === accountId) {
          account.childs = childAccounts;
        }
      });
    });
  }
}

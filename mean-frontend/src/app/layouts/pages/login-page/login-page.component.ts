import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserService } from '../../../services/user.service';
import { MatDialog } from "@angular/material";

import _ from 'lodash';

import { TwoFactorAuthenticationDialogComponent } from "../../dialogs/two-factor-authentication-dialog/two-factor-authentication-dialog.component";
import { AuthenticationService } from 'src/app/services/authentication.service';
import { resolve } from 'url';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  private forgotPwLink: string;
  private email: string;
  private password: string;
  private isHover: boolean = false;

  constructor(private authenticationService: AuthenticationService, private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
    this.email = 'tranhoangminhptit@gmail.com';
    this.password = '123';
    this.onSigninSubmit();
  }

  onEmailChanged(value) {
    this.email = value;
  }

  onPasswordChanged(value) {
    this.password = value;
  }

  hoverIn() {
    this.isHover = true;
  }

  hoverOut() {
    this.isHover = false;
  }

  onSigninSubmit() {
    return new Promise((resolve, reject) => {
      return this.authenticationService.authenticate(this.email, this.password)
        .subscribe((res: any) => {
          if (res.status !== 401 && !_.isEmpty(res)) {
            let result: any = {
              ...res,
              verified: true
            };

            if (res.is2FA) {
              return this.dialog.open(TwoFactorAuthenticationDialogComponent, {
                data: {
                  code2FA: '',
                }
              })
                .afterClosed()
                .subscribe(rs => {
                  result.code2FA = rs.code2FA;

                  resolve(result);
                });
            }

            resolve(result);
          }
        });
    })
      .then((res: any) => {
        return new Promise((resolve, reject) => {
          if (res.is2FA) {
            return this.authenticationService.authenticate2FA(res._id, res.code2FA)
              .subscribe((result: any) => {
                if (result.status == 401 || !result) {
                  res.verified = false;
                }

                resolve(res);
              });
          }

          resolve(res)
        });
      })
      .then((res: any) => {
        if (res.verified) {
          window.localStorage.setItem('userId', res._id);
          window.localStorage.setItem('accountId', res.account._id);
          window.localStorage.setItem('is2FA', res.is2FA);
          window.localStorage.setItem('username', res.firstname + " " + res.lastname);

          this.router.navigate(['/main/accounts']);
        }
      });
  }
}

import _ from 'lodash';
import QRCode from 'qrcode';
import speakeasy from 'speakeasy';
import { Router } from '@angular/router';
import { MatDialog } from "@angular/material";
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { UserService } from '../../../services/user.service';
import { QrcodeTfaDialogComponent } from '../../dialogs/qrcode-tfa-dialog/qrcode-tfa-dialog.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  private userId: any;
  private isShowAccountMenu: boolean = false;
  private is2FA: boolean;
  private accountNumber: number;
  private username: string;

  constructor(private dialog: MatDialog, private userService: UserService, private router: Router) { }

  ngOnInit() {
    this.userId = window.localStorage.getItem('userId');
    this.is2FA = window.localStorage.getItem('is2FA') === 'true';
    this.accountNumber = parseInt(window.localStorage.getItem('accountNumber'));
    this.username = window.localStorage.getItem('username')
  }

  mouseAccountEnter() {
    this.isShowAccountMenu = true;
  }

  mouseAccountLeave() {
    this.isShowAccountMenu = false;
  }

  on2FAChanged(value) {
    if (value.checked) {
      var secret = speakeasy.generateSecret({ length: 20 });

      QRCode.toDataURL(secret.otpauth_url, (err, imageData) => {
        if (!_.isEmpty(imageData)) {
          this.dialog.open(QrcodeTfaDialogComponent, {
            autoFocus: false,
            data: {
              qrCode: imageData,
            }
          })
            .afterClosed()
            .subscribe(result => {
              if (result === "ok") {
                this.userService.update(this.userId,
                  {
                    secret: secret.base32
                  }
                )
                  .subscribe((res: any) => {
                    if (!_.isEmpty(res)) {
                      window.localStorage.setItem('is2FA', 'true');
                    }
                  });
              }
            });
        }
      });
    } else {
      this.userService.update(this.userId,
        {
          secret: ''
        }
      )
        .subscribe((res: any) => {
          window.localStorage.setItem('is2FA', 'false');
        });
    }
  }

  onSignOutClicked() {
    window.localStorage.clear();
    this.router.navigate(['/signin']);
  }

}

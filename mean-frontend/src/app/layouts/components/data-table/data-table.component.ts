import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {
  @Input("_headers") headers: any = [];
  @Input("_values") values: any = [];
  @Input("_actions") actions: any = [];
  @Input("_searchFields") searchFields: any = [];

  @Output() onAction: EventEmitter<any> = new EventEmitter<any>();
  @Output() onSearch: EventEmitter<any> = new EventEmitter<any>();
  @Output() onChild: EventEmitter<any> = new EventEmitter<any>();

  private isChildButtonShow: boolean;
  private isShowChildRow: boolean = false;

  constructor() { }

  ngOnInit() {
    // this.isChildButtonShow = this.values.childCound > 0;
  }

  /**
   * 
   * @author Minh Tran
   * @param actionIndex 
   * @param id 
   */
  public onActionTrigger(actionIndex: any, id: any): any {
    this.onAction.emit({
      actionIndex: actionIndex,
      id: id
    });
  }

  onSubmit(data) {
    this.onSearch.emit(data);
  }

  onShowChildTrigger(id) {
    this.onChild.emit(id);
  }

}

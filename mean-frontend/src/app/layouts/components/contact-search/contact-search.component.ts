import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-search',
  templateUrl: './contact-search.component.html',
  styleUrls: ['./contact-search.component.scss']
})
export class ContactSearchComponent implements OnInit {
  private isShowSearchForm: boolean = false;
  private segmentOptions: any = ["All Contacts", "Transaction", "Gender", "ABC"];
  private searchTypeOptions: any = ["Match all of the search criteria", "Match one or more of the search criteria"]
  private commonConditions: any = ["All", "Equals", "Does not equal", "Contains", "Does not contain", "Start with", "Does not start with", "Ends with", "Does not end with", "Is empty", "Is not empty"];

  constructor() { }

  ngOnInit() {
  }

  onSearchFormOpen() {
    this.isShowSearchForm = !this.isShowSearchForm;
  }

  onSearchFormChanged(value, key) {
  }

  onSubmit() {
  }

}

import { ViewEncapsulation } from '@angular/core';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddAccountDialogData } from '../../../others/IDialog';

@Component({
  selector: 'app-add-account-dialog',
  templateUrl: './add-account-dialog.component.html',
  styleUrls: ['./add-account-dialog.component.scss']
})
export class AddAccountDialogComponent implements OnInit {
  private isShowPricing: boolean = false;
  private isPricingRequired: boolean = false;

  constructor(public dialogRef: MatDialogRef<AddAccountDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddAccountDialogData) { }

  ngOnInit() {

  }

  onSubmit(data) {
    if (data.accountName && data.ownerEmail && data.country && data.timezone) {
      if (data.currency
        && data.monthlySubscriptionFee
        && data.emailInclusion
        && data.smsInclusion
        && data.contactInclusion
        && data.payBy
        && data.emailBasePrice
        && data.smsBasePrice
        && data.contactBasePrice
        && this.isShowPricing) {
        this.dialogRef.close(data);
      }
      this.isShowPricing = true;
    }
  }

}

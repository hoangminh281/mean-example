import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QrcodeTfaDialogComponent } from './qrcode-tfa-dialog.component';

describe('QrcodeTfaDialogComponent', () => {
  let component: QrcodeTfaDialogComponent;
  let fixture: ComponentFixture<QrcodeTfaDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QrcodeTfaDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QrcodeTfaDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { QrcodeTfaDialogComponentData } from '../../../others/IDialog';

@Component({
  selector: 'app-qrcode-tfa-dialog',
  templateUrl: './qrcode-tfa-dialog.component.html',
  styleUrls: ['./qrcode-tfa-dialog.component.scss']
})
export class QrcodeTfaDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<QrcodeTfaDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: QrcodeTfaDialogComponentData) { }

  ngOnInit() {
  }

}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { AddCampaignDialogData } from '../../../others/IDialog';

@Component({
  selector: 'app-add-campaign-dialog',
  templateUrl: './add-campaign-dialog.component.html',
  styleUrls: ['./add-campaign-dialog.component.scss']
})
export class AddCampaignDialogComponent implements OnInit {
  private isEmail: boolean = false;
  private isSms: boolean = false;
  private folders:any = ["123", "charged", "fee"];

  constructor(public dialogRef: MatDialogRef<AddCampaignDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AddCampaignDialogData) { }

  ngOnInit() {
  }

  onTypeActive(type) {
    if (type === 'email') {
      this.isEmail = true;
      this.isSms = false;
    } else if (type === 'sms') {
      this.isEmail = false;
      this.isSms = true;
    }
    this.data.type = type;
  }

  onSubmit(data){
    this.dialogRef.close();
  }
}

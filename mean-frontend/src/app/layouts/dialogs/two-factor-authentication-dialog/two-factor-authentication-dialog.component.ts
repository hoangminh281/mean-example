import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TwoFactorAuthenticationDialogData } from '../../../others/IDialog';

@Component({
  selector: 'app-two-factor-authentication-dialog',
  templateUrl: './two-factor-authentication-dialog.component.html',
  styleUrls: ['./two-factor-authentication-dialog.component.scss']
})
export class TwoFactorAuthenticationDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TwoFactorAuthenticationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: TwoFactorAuthenticationDialogData) { }

  ngOnInit() {
  }

  onSubmit(data) {
    if (data.code2FA !== '') {
      this.dialogRef.close(data);
    }
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignStep3FormComponent } from './add-campaign-step3-form.component';

describe('AddCampaignStep3FormComponent', () => {
  let component: AddCampaignStep3FormComponent;
  let fixture: ComponentFixture<AddCampaignStep3FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignStep3FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignStep3FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

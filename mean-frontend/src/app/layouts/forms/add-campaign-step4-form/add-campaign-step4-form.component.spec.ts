import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignStep4FormComponent } from './add-campaign-step4-form.component';

describe('AddCampaignStep4FormComponent', () => {
  let component: AddCampaignStep4FormComponent;
  let fixture: ComponentFixture<AddCampaignStep4FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignStep4FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignStep4FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

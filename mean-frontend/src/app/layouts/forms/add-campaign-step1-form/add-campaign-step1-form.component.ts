import { Component, OnInit, Inject, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SetupCampaignFormData } from '../../../others/IDialog';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-campaign-step1-form',
  templateUrl: './add-campaign-step1-form.component.html',
  styleUrls: ['./add-campaign-step1-form.component.scss']
})
export class AddCampaignStep1Form implements OnInit {
  @Output()
  onSubmitted: EventEmitter<string> = new EventEmitter<string>();

  constructor(public dialogRef: MatDialogRef<AddCampaignStep1Form>,
    @Inject(MAT_DIALOG_DATA) public data: SetupCampaignFormData) { }

  ngOnInit() {
  }

  onSubmit(data) {
    this.onSubmitted.emit(data);
  }

}

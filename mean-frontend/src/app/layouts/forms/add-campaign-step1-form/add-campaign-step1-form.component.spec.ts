import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignStep1Form } from './add-campaign-step1-form.component';

describe('add-campaign-step1-form', () => {
  let component: AddCampaignStep1Form;
  let fixture: ComponentFixture<AddCampaignStep1Form>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignStep1Form ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignStep1Form);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

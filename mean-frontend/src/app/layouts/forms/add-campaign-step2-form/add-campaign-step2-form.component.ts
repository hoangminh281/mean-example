import { Component, OnInit, ViewChild, ElementRef, Renderer2 } from '@angular/core';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

@Component({
  selector: 'app-add-campaign-step2-form',
  templateUrl: './add-campaign-step2-form.component.html',
  styleUrls: ['./add-campaign-step2-form.component.scss']
})
export class AddCampaignStep2FormComponent implements OnInit {
  @ViewChild('template') dataContainer: ElementRef;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  onInputClick() {
    console.log('vao')
  }

  drop(event: CdkDragDrop<string[]>) {
    switch (event.previousContainer.id) {
      case 'text':
        const textString = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum';
        const textArea = this.renderer.createElement('textarea');
        const text = this.renderer.createText(textString);

        this.renderer.setAttribute(textArea, 'class', 'form-control');
        this.renderer.appendChild(textArea, text);
        this.renderer.appendChild(this.dataContainer.nativeElement, textArea);
        break;
      case 'image':
        const div = this.renderer.createElement('div');
        const img = this.renderer.createElement('img');

        this.renderer.setAttribute(img, 'src', '../../../assets/images/imgpsh_fullsize.jpg');
        this.renderer.setAttribute(img, 'class', 'full-width');
        this.renderer.appendChild(div, img);
        this.renderer.appendChild(this.dataContainer.nativeElement, div);
        break;
      case 'heading':
        const h3 = this.renderer.createElement('h3');
        const heading = this.renderer.createText('Heading');

        this.renderer.appendChild(h3, heading);
        this.renderer.appendChild(this.dataContainer.nativeElement, h3);
        break;
      case 'button':
        const div2 = this.renderer.createElement('div');
        const button = this.renderer.createElement('button');
        const text2 = this.renderer.createText('Button');

        this.renderer.setAttribute(button, 'class', 'btn button');
        this.renderer.appendChild(button, text2);
        this.renderer.appendChild(div2, button);
        this.renderer.appendChild(this.dataContainer.nativeElement, div2);
        break;
    }
    console.log(event.previousContainer.id);
  }

}

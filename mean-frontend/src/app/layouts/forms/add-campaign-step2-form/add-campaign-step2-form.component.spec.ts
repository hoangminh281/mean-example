import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignStep2FormComponent } from './add-campaign-step2-form.component';

describe('AddCampaignStep2FormComponent', () => {
  let component: AddCampaignStep2FormComponent;
  let fixture: ComponentFixture<AddCampaignStep2FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignStep2FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignStep2FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignStep5FormComponent } from './add-campaign-step5-form.component';

describe('AddCampaignStep5FormComponent', () => {
  let component: AddCampaignStep5FormComponent;
  let fixture: ComponentFixture<AddCampaignStep5FormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignStep5FormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignStep5FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

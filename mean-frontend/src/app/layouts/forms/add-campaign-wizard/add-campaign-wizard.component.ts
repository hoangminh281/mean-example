import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-add-campaign-wizard',
  templateUrl: './add-campaign-wizard.component.html',
  styleUrls: ['./add-campaign-wizard.component.scss']
})
export class AddCampaignWizardComponent implements OnInit {
  private routes: any = ['Set up', 'Content', 'Recipients', 'Schedule', 'Confirm'];
  private redirectRouteIndex: any = 0;

  constructor() { }

  ngOnInit() {
  }

  saveAndClose(data) {
  }

  onSubmit(data) {
    switch (this.redirectRouteIndex) {
      case this.routes[0]:
        break;
      case this.routes[1]:
        break;
      case this.routes[2]:
        break;
      case this.routes[3]:
        break;
      case this.routes[4]:
        break;
    }
    this.redirectRouteIndex++;
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCampaignWizardComponent } from './add-campaign-wizard.component';

describe('AddCampaignWizardComponent', () => {
  let component: AddCampaignWizardComponent;
  let fixture: ComponentFixture<AddCampaignWizardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCampaignWizardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCampaignWizardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

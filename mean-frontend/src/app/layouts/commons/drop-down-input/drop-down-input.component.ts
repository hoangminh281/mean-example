import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-drop-down-input',
  templateUrl: './drop-down-input.component.html',
  styleUrls: ['./drop-down-input.component.scss']
})
export class DropDownInputComponent implements OnInit {
  @Input("_options") options;
  @Input("_value") value;
  @Input("_title") title;

  @Output()
  onChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

}

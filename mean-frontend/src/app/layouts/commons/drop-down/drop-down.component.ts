import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-drop-down',
  templateUrl: './drop-down.component.html',
  styleUrls: ['./drop-down.component.scss'],

})
export class DropDownComponent implements OnInit {
  @Input("_options") options;
  @Input("_value") value;
  @Input("_title") title;

  @Output()
  onChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onSelect(option) {
    this.value = option;
    this.onChanged.emit(option);
  }
}

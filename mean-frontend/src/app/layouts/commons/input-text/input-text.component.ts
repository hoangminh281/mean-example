import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit {
  @Input('_placeholder') placeHolder: string;
  @Input('_required') required: boolean;

  @Output()
  onChanged: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onKey(event: any) {
    const value = event.target.value;
    this.onChanged.emit(value);
  }
}

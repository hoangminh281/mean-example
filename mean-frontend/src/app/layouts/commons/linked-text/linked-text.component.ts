import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-linked-text',
  templateUrl: './linked-text.component.html',
  styleUrls: ['./linked-text.component.scss']
})
export class LinkedTextComponent implements OnInit {
  @Input() _content: any;
  @Input() _link: string;
  @Input() _color: string;
  @Input() _target: string; //enum: blank|self|parent|top|framename

  constructor() { }

  ngOnInit() {
  }

}

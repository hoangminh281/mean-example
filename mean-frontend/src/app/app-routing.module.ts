import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './layouts/pages/login-page/login-page.component';
import { AccountPageComponent } from './layouts/pages/account-page/account-page.component';
import { UserPageComponent } from './layouts/pages/user-page/user-page.component';
import { ContactPageComponent } from './layouts/pages/contact-page/contact-page.component';
import { CampaignPageComponent } from './layouts/pages/campaign-page/campaign-page.component';
import { AddCampaignStep1Form } from './layouts/forms/add-campaign-step1-form/add-campaign-step1-form.component';

const routes: Routes = [
  // { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'signin', component: LoginPageComponent },
  {
    path: 'main', children: [
      {
        path: 'accounts', component: AccountPageComponent
      },
      {
        path: 'users', component: UserPageComponent
      },
      {
        path: 'contacts', component: ContactPageComponent
      },
      {
        path: 'campaigns', component: CampaignPageComponent,
        children: [
          {
            path: 'setup', component: AddCampaignStep1Form
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule { }

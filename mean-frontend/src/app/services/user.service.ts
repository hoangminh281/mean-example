import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = 'http://localhost:3000';
  private userApi = '/user';
  private findApi = '/find';

  constructor(private http: HttpClient) { }

  findByOptions(options) {
    return this.http.post(this.baseUrl + this.userApi + this.findApi, options, httpOptions);
  }

  update(userId, updateObject) {
    return this.http.put(this.baseUrl + this.userApi + "/" +  userId, updateObject, httpOptions);
  }

  create(newUser) {
    return this.http.post(this.baseUrl + this.userApi, newUser, httpOptions);
  }
}

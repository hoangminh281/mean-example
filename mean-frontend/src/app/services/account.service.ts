import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private baseUrl = 'http://localhost:3000';
  private accountApi = '/account/';
  private childApi = "/child";

  constructor(private http: HttpClient) { }

  getChildAccount(accountId) {
    return this.http.get(this.baseUrl + this.accountApi + accountId + this.childApi);
  }

  create(account) {
    return this.http.post(this.baseUrl + this.accountApi, {
      account: {
        "email_inclusion": account.emailInclusion,
        "sms_inclusion": account.smsInclusion,
        "contact_inclusion": account.contactInclusion,
        "startup_fee": account.startupFee,
        "name": account.accountName,
        "country": account.country,
        "timezone": account.timezone,
        "currency": account.currency,
        "monthly_subscription_fee": account.monthlySubscriptionFee,
        "email_fee": account.emailBasePrice,
        "sms_fee": account.smsBasePrice,
        "contact_fee": account.contactBasePrice,
        "parent": account.parent
      },
      user: {
        "email": account.ownerEmail
      }
    }, httpOptions);
  }

  update(accountId, account) {
    return this.http.put(this.baseUrl + this.accountApi + accountId, {
      "email_inclusion": account.emailInclusion,
      "sms_inclusion": account.smsInclusion,
      "contact_inclusion": account.contactInclusion,
      "startup_fee": account.startupFee,
      "name": account.accountName,
      "country": account.country,
      "timezone": account.timezone,
      "currency": account.currency,
      "monthly_subscription_fee": account.monthlySubscriptionFee,
      "email_fee": account.emailBasePrice,
      "sms_fee": account.smsBasePrice,
      "contact_fee": account.contactBasePrice,
      "parent": account.parent
    }, httpOptions);
  }

  findById(accountId) {
    return this.http.get(this.baseUrl + this.accountApi + accountId, httpOptions);
  }
}
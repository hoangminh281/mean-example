import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private baseUrl = 'http://localhost:3000';
  private contactApi = '/contact';
  private findApi = '/find';

  constructor(private http: HttpClient) { }

  findByOptions(options) {
    return this.http.post(this.baseUrl + this.contactApi + this.findApi, options, httpOptions);
  }

  update(contactId, updateObject) {
    return this.http.put(this.baseUrl + this.contactApi + "/" +  contactId, updateObject, httpOptions);
  }

  create(newContact) {
    return this.http.post(this.baseUrl + this.contactApi, newContact, httpOptions);
  }
}

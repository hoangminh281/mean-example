import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private baseUrl = 'http://localhost:3000';
  private authApi = '/auth/';
  private signinApi = 'signin';
  private signin2FAApi = 'authenticate2fa';

  constructor(private http: HttpClient) { }

  authenticate(email, password) {
    return this.http.post(this.baseUrl + this.authApi + this.signinApi, {
      email,
      password
    }, httpOptions);
  }

  authenticate2FA(userId, code2FA) {
    return this.http.post(this.baseUrl + this.authApi + this.signin2FAApi, {
      userId,
      code2FA
    }, httpOptions);
  }
}

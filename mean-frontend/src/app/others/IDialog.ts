export interface AddAccountDialogData {
    accountName: string;
    ownerEmail: string;
    country: string;
    timezone: string;

    currency: string;
    monthlySubscriptionFee: string;
    startupFee: string;
    emailInclusion: string;
    smsInclusion: string;
    contactInclusion: string;
    payBy: string;
    emailBasePrice: string;
    smsBasePrice: string;
    contactBasePrice: string;
    parent: string;
}

export interface AddUserDialogData {
    email: string;
    role: string;
}

export interface AddContactDialogData {
    firstName: string;
    surname: string;
    email: string;
    mobile: string;
    dateOfBirth: string;
    gender: string;
    hobbies: string;
    date: string;
}

export interface AddCampaignDialogData {
    type: string;
    folder: string;
    name: string;
}

export interface SetupCampaignFormData {
    nameSubject: string;
    nameFrom: string;
    emailFrom: string;
    emailReplies: string;
    isSend: boolean;
}

export interface TwoFactorAuthenticationDialogData {
    code2FA: string;
}

export interface QrcodeTfaDialogComponentData {
    qrCode: string;
}
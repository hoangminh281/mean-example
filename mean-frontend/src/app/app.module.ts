import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';

//common layout
import { BannerComponent } from './layouts/components/banner/banner.component';
import { HeaderComponent } from './layouts/components/header/header.component';
import { DropDownComponent } from './layouts/commons/drop-down/drop-down.component';
import { MainMenuComponent } from './layouts/components/main-menu/main-menu.component';
import { InputTextComponent } from './layouts/commons/input-text/input-text.component';
import { DataTableComponent } from './layouts/components/data-table/data-table.component';
import { LinkedTextComponent } from './layouts/commons/linked-text/linked-text.component';
import { FloatButtonComponent } from './layouts/commons/float-button/float-button.component';
import { ContactSearchComponent } from './layouts/components/contact-search/contact-search.component';

//dialog layout
import { AddUserDialogComponent } from './layouts/dialogs/add-user-dialog/add-user-dialog.component';
import { AddAccountDialogComponent } from './layouts/dialogs/add-account-dialog/add-account-dialog.component';
import { AddContactDialogComponent } from './layouts/dialogs/add-contact-dialog/add-contact-dialog.component';
import { AddCampaignDialogComponent } from './layouts/dialogs/add-campaign-dialog/add-campaign-dialog.component';
import { TwoFactorAuthenticationDialogComponent } from './layouts/dialogs/two-factor-authentication-dialog/two-factor-authentication-dialog.component';
import { QrcodeTfaDialogComponent } from './layouts/dialogs/qrcode-tfa-dialog/qrcode-tfa-dialog.component';

//form layout
import { AddCampaignStep1Form } from './layouts/forms/add-campaign-step1-form/add-campaign-step1-form.component';
import { AddCampaignStep2FormComponent } from './layouts/forms/add-campaign-step2-form/add-campaign-step2-form.component';
import { AddCampaignStep3FormComponent } from './layouts/forms/add-campaign-step3-form/add-campaign-step3-form.component';
import { AddCampaignStep4FormComponent } from './layouts/forms/add-campaign-step4-form/add-campaign-step4-form.component';
import { AddCampaignStep5FormComponent } from './layouts/forms/add-campaign-step5-form/add-campaign-step5-form.component';

//main layout
import { AppComponent } from './app.component';
import { UserPageComponent } from './layouts/pages/user-page/user-page.component';
import { LoginPageComponent } from './layouts/pages/login-page/login-page.component';
import { AccountPageComponent } from './layouts/pages/account-page/account-page.component';
import { ContactPageComponent } from './layouts/pages/contact-page/contact-page.component';
import { CampaignPageComponent } from './layouts/pages/campaign-page/campaign-page.component';
import { AddCampaignWizardComponent } from './layouts/forms/add-campaign-wizard/add-campaign-wizard.component';

import {
  MatDialogModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatCheckboxModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatMenuModule
} from '@angular/material';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { DropDownInputComponent } from './layouts/commons/drop-down-input/drop-down-input.component';

const modules = [
  MatDialogModule,
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule,
  MatCheckboxModule,
  MatRadioModule,
  MatSlideToggleModule,
  MatMenuModule,
  BrowserAnimationsModule,
  DragDropModule
];

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    InputTextComponent,
    LinkedTextComponent,
    MainMenuComponent,
    HeaderComponent,
    BannerComponent,
    DataTableComponent,
    AccountPageComponent,
    FloatButtonComponent,
    AddAccountDialogComponent,
    UserPageComponent,
    AddUserDialogComponent,
    ContactPageComponent,
    ContactSearchComponent,
    DropDownComponent,
    AddContactDialogComponent,
    CampaignPageComponent,
    AddCampaignDialogComponent,
    AddCampaignStep1Form,
    AddCampaignWizardComponent,
    AddCampaignStep2FormComponent,
    AddCampaignStep3FormComponent,
    AddCampaignStep4FormComponent,
    AddCampaignStep5FormComponent,
    TwoFactorAuthenticationDialogComponent,
    QrcodeTfaDialogComponent,
    DropDownInputComponent,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    ...modules,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [
    AddUserDialogComponent,
    AddAccountDialogComponent,
    AddContactDialogComponent,
    AddCampaignDialogComponent,
    AddCampaignStep1Form,
    AddCampaignWizardComponent,
    TwoFactorAuthenticationDialogComponent,
    QrcodeTfaDialogComponent
  ]
})
export class AppModule { }
